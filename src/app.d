import std.algorithm;
import std.array;
import std.conv;
import std.file;
import std.format;
import std.json;
import std.path;
import std.regex;
import std.stdio;
import std.string;

void main(string[] args) {
	if (args.length < 2) {
		writeln("I need moar args!");
		return;
	}

	foreach(string path; args[1..$]) {
		validateStateMachineFromFilePath(path);
	}
}

bool validateStateMachineFromFilePath(string file_path) {
	auto my_file = File(file_path);
	auto text = my_file.byLine().joiner("\n");
	auto json_data = std.json.parseJSON(text);

	JSONValue[] state_machines = [];

	foreach(string key, ref JSONValue value; json_data["Resources"]) {
	if(value["Type"].str == "AWS::StepFunctions::StateMachine") {
			state_machines ~= value;
		}
	}

	writeln("Validating " ~ file_path ~ "...");
	if(validateStateMachines(state_machines)) {
		writeln(`\o/`);
		return true;
	} else {
		writeln("sad...");
		return false;
	}
}

bool validateStateMachines(JSONValue[] state_machines) {
	bool result = true;
	foreach(state_machine; state_machines) {
		result &= validateStateMachine(state_machine);
	}

	return result;
}

bool validateStateMachine(JSONValue state_machine) {
	bool result = true;

	auto clean = cleanStateMachine(state_machine);

	assert(clean["States"].type == JSONType.object);
	foreach(string key, JSONValue value; clean["States"]) {
		result &= validateStep(key, value);
	}

	return result;
}

JSONValue cleanStateMachine(JSONValue state_machine) {

	JSONValue test = state_machine;
	assert(test.type == JSONType.object);

	test = test["Properties"];
	assert(test.type == JSONType.object);

	test = test["DefinitionString"];
	assert(test.type == JSONType.object);

	test = test["Fn::Join"];
	assert(test.type == JSONType.array);

	/*
	For some stupid reason, Fn::Join is an array of ["", [<things>]],
	where [<things>] is the actual array that we want.
	We assign that to array_data.
	*/
	test = test.array[1];
	assert(test.type == JSONType.array);

	auto result = clean(test);
	return std.json.parseJSON(result);
}

string clean(JSONValue state_machine) {
	auto buf = appender!string;

	switch(state_machine.type) {
		case JSONType.string:
			buf ~= state_machine.str;
			break;
		case JSONType.object:
			foreach(string key, JSONValue value; state_machine) {
				if(key == "Ref") {
					buf ~= "aws:";
					continue;
				} else {
					buf ~= clean(value);
				}
			}
			break;
		case JSONType.array:
			foreach(JSONValue value; state_machine.array) {
				buf ~= clean(value);
			}
			break;
		default:
		assert(false);
	}

	return buf.data;
}

bool validateStep(string key, JSONValue value){
	auto len = key.length;
	bool result = len <=80;
	string pass = result ? "[PASS]" : "[FAIL]";

	writeln(format(`	%s	%s	%s`, pass, len, key));
	return result;
}

unittest {
	string test_file = "assets/aws-stepfunctions-integ.template.json";
	writeln("Testing validation of " ~ test_file);

	assert(validateStateMachineFromFilePath(test_file) == true);
}
